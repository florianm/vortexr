# vortexr
An R package for Post Vortex Simulation Analysis

## Quickstart
Install the package from version control from within R:
```
library(devtools)
install_bitbucket("vortexr", "florianm")
```

## Documentation
Read the vignette and the function help for more information.
